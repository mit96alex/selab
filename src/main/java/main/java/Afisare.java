package main.java;

import java.io.*;
import java.util.Scanner;

/**
 * Aceasta clasa este folosita pentru implementarea optiunilor 1, 2 si 8.
 * In principal este o clasa de afisare continut de fisier sau a rezumatului unui film.
 *
 * @author Mitu Alexandru
 */
public class Afisare {
    private Tools util=new Tools(); //clasa include cerearea de api si parsere

    /**
     * Functie folosita pentru afisarea filmelor dupa ID.
     * In principal fisierele sunt: "vizionate.txt" sau "devizionat.txt".
     * Este folosita la optiunile 1 si 2.
     * @param nume_fisier
     * @throws FileNotFoundException
     */
    public void afisare_filme(String nume_fisier) throws FileNotFoundException
    {
        File readfile = new File(nume_fisier); //deschidere fisier pentru citire
        Scanner myReader = new Scanner(readfile);
        while (myReader.hasNextLine()) {
            String id = myReader.nextLine();  //preluare fiecare id din fisier.
            String jsonContent =util.cerere_api(1,id); //preluare continutul json cu ajutorul id-ului
            util.parse_pt_titlu(jsonContent);
        }
    }

    /**
     * Functie folosita pentru afisarea rezumatului unui film dupa ID.
     * Aceasta functie se ajuta de clasa Tools pentru cerere api si parsare.
     * Este folosita la optiunea 8.
     *
     * @throws IOException
     */
    public void afisare_rezumat() throws IOException //functie pentru afisarea rezumatului unui film dupa id.
    {
        String id;
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        System.out.print("Introduceti Id:");
        id=br.readLine(); //introducerea ID-ului filmului pentru rezumat
        String jsonContent =util.cerere_api(1,id); //preluare continutul json cu ajutorul id-ului
        util.parse_pt_rezumat(jsonContent);
    }
}