package main.java;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Aceasta clasa este folosita pentru scrierea ID-urilor in fisier
 * Este folosita la optiunile 6 si 7.
 *
 * @author Mitu Alexandru
 */
public class ADD {

    private String id; //pentru introducerea ID-ului filmului

    /**
     * Functie ajuta la introducerea ID-urilor in fisierele "devizionat.txt" si "vizionate.txt".
     * Fisierul este dat ca parametru al functiei.
     * Este folosita pentru optiunile 6 si 7.
     *
     * @param nume_fisier
     * @throws IOException
     */
    public void viz(String nume_fisier) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        System.out.print("Introduceti Id:");
        id=br.readLine(); //introducerea ID-ului filmului pentru adaugare in fisier
        try {
            FileWriter myWriter = new FileWriter(nume_fisier,true); //deschiderea fisierului pentru a scrie id-ul filmului
            myWriter.write(id+"\n"); //scriere id in fisier
            myWriter.close();
            System.out.println("Done.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }




}
