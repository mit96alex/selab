package main.java;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Aceasta clasa este folosita in ajutorul celorlalte clase.
 * Avem functia pentru cerere de api.
 * Avem mai multe functii de parsare cu diferite afisari pentru mai multe optiuni.
 *
 * @author Mitu Alexandru
 */

public class Tools {

    /**
     * Functie folosita pentru a faca o cerere catre APi.
     * Primul paramentru este folosit pentru:
     * 1-pt id
     * 2-pt titlu
     * 3-pt cautare dupa cuvinte cheie
     *
     * @param n
     * @param x
     * @return
     */
    public String cerere_api(int n,String x)
    {
        String apiAddress = "http://www.omdbapi.com";// adresa API-ului
        String apiKey = "apikey=21af130d";// cheia primită pe email
        StringBuilder content = new StringBuilder();
        String requestString = null;
        // link-ul finale către care vom face cererea. Contine parametrii GET necesari pentru cererea noastră.
        if(n==1) {
            requestString = apiAddress + "?" + "i=" + x + "&" + apiKey; //cautare dupa id
        }
        if(n==2) {
            requestString = apiAddress + "?" + "t=" + x + "&" + apiKey; //cautare dupa titlu
        }
        if(n==3) {
            requestString = apiAddress + "?" + "s=" + x + "&" + apiKey; //cautare dupa cuvinte cheie
        }

        try {
            // creăm un obiec URL pe baza request-string-ului
            URL url = new URL(requestString);

            // Deschidem conexiunea
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            // setăm metoda HTTP
            con.setRequestMethod("GET");

            // trimitem cererea si asteptam raspunsul. status va conține codul intors de cererea noastră(ex. 200).
            int status = con.getResponseCode();

            // Creăm un BufferedReader pentru a citi informația de la linkul respectiv
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));

            String inputLine;

            // Citim toate liniile si le appendu-im in content

            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }

            // content va contine practic datele in format json. Il vom trimite catre parser-ul de json ulterior.

            // Inchidem reader-ul si conexiunea
            in.close();
            con.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String jsonContent = content.toString();
        return jsonContent;

    }

    /**
     * Functie folosita pentru afisarea de titlu si id.
     * Este folosit pentru optiunile 1 si 2
     * @param jsonContent
     */
    public void parse_pt_titlu(String jsonContent)
    {
        // Parsam continutul si il stocăm într-un obiect Json
        JsonObject object = Json.parse(jsonContent).asObject();

        // Obținem și afișăm valoarea câmpului "Titlu"
        String titlu=object.getString("Title","Not Found");
        System.out.print(titlu);

        // Obținem și afișăm valoarea câmpului "imdbID"
        String id_parse=object.getString("imdbID","Not Found");
        System.out.println(" - "+id_parse);
    }

    /**
     * Functie folosita pentru afisarea rezumatului.
     * Este folosita pentru optiunea 8.
     * @param jsonContent
     */
    public void parse_pt_rezumat(String jsonContent)
    {
        // Parsam continutul si il stocăm într-un obiect Json
        JsonObject object = Json.parse(jsonContent).asObject();
        //Obținem și afișăm valoarea câmpului "Plot"
        String rezumat=object.getString("Plot","Not Found");
        System.out.println(rezumat);
    }

    /**
     * Functie folosita pentru a afisa mai multe campuri.
     * Este folosita pentru optiunile 3 si 4.
     * @param jsonContent
     */
    public void parse_pt_cautare(String jsonContent)
    {
        // Parsam continutul si il stocăm într-un obiect Json
        JsonObject object = Json.parse(jsonContent).asObject();

        // Obținem și afișăm valoarea câmpului "Actors"
        String actors = object.getString("Actors", "Not Found");
        System.out.println("Actori: " + actors);
        //Obținem și afișăm valoarea câmpului "Released"
        String an = object.getString("Released", "Not Found");
        System.out.println("An Aparitie: " + an);
        //Obținem și afișăm valoarea câmpului "Genre"
        String gen = object.getString("Genre", "Not Found");
        System.out.println("Gen: " + gen);
        //Obținem și afișăm valoarea câmpului "Awards"
        String premii= object.getString("Awards", "Not Found");
        System.out.println("Premii: " + premii);
        //Trebuie sa scoatem ratingurile din "Rating" cu ajutorul Array-ul de obiecte
        System.out.println("Rating-uri: ");
        // Obținem Array-ul de obiecte corespunzatoare câmpului "Ratings"
        JsonArray ratings = object.get("Ratings").asArray();
        // Iterăm printre valori
        for (JsonValue item : ratings) {
            // Obținem și afișăm valoarea câmpului "Source"
            String source = item.asObject().getString("Source", "Not Found");
            System.out.print(source +" - ");
            // Obținem și afișăm valoarea câmpului "Value"
            String value = item.asObject().getString("Value", "Not Found");
            System.out.println(value);
        }
        //Obținem și afișăm valoarea câmpului "imdbRating"
        String imdbrat= object.getString("imdbRating", "Not Found");
        System.out.println("IMDB Rating: " + imdbrat);
        //Pentru formarea link-ului avem nevoie sa obtinem valoarea campului imdbID
        //Dupa formam un string legand https://www.imdb.com/title/ + id
        String Link="https://www.imdb.com/title/"+object.getString("imdbID","Not found");
        System.out.println("Link pagina Imdb: "+Link);
    }

    /**
     * Functie folosita pentru optiunea 5.
     * @param jsonContent
     */
    public void parse_cheie(String jsonContent)
    {
        // Parsam continutul si il stocăm într-un obiect Json
        JsonObject object = Json.parse(jsonContent).asObject();
        // Formam o lista de obiecte cu toate filmele
        JsonArray cautari = object.get("Search").asArray();
        for (JsonValue item :cautari ) {
            // Obținem și afișăm valoarea câmpului "Title"
            String titlu = item.asObject().getString("Title", "Not Found");
            System.out.print(titlu + " - ");
            // Obținem și afișăm valoarea câmpului "Year"
            String an = item.asObject().getString("Year", "Not Found");
            System.out.print(an + " - ");
            // Obținem și afișăm valoarea câmpului "imdbID"
            String id_parse = item.asObject().getString("imdbID", "Not Found");
            System.out.print(id_parse + " - ");
            // Obținem și afișăm valoarea câmpului "Type"
            String tip = item.asObject().getString("Type", "Not Found");
            System.out.println(tip);
        }
    }
}
