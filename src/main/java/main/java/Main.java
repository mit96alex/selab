package main.java;

import java.io.IOException;
import java.util.Scanner;

/**
 * Clasa Main este folosita cu scopul meniului programului, de unde iti alegi optiunea dorita.
 * Aici se folosesc Clasele Afisare, Cautare si ADD.
 * Meniul este incorporat intru switch, iar switch-ul este introdus intr-un loop.
 *
 * @author Mitu Alexandru
 */
public class Main {

    private static ADD v; //clasa folosita pentru optiunile 6 si 7
    private static Afisare a; // clasa folosita pentru optiunile 1, 2 si 8
    private static Cauta c; //clasa folosita pentu optinile 3, 4 si 5

    public static void main(String[] args) throws IOException {

        v=new ADD();
        a=new Afisare();
        c=new Cauta();
        int loop=1; //variabila pentru formarea de bucla
        Scanner scan=new Scanner(System.in);

        System.out.println("Alegeti o optiune:\n" +
                "1. Afisare filme vizionate\n" +
                "2. Afisare filme de vizionat\n" +
                "3. Cautare film dupa titlu\n" +
                "4. Cautare film dupa id IMDB\n" +
                "5. Cautare film dupa cuvant cheie\n" +
                "6. Adauga film in lista de vizionate\n" +
                "7. Adauga film in lista de viitoare vizionari\n" +
                "8. Afisare rezumat film\n" +
                "9. Exit");

        while(loop==1) {
            System.out.print("Optiune: ");
            int comanda=scan.nextInt(); //folosit pentru a alege o comanda
            switch (comanda) {
                case 1:
                    a.afisare_filme("vizionate.txt");
                    break;
                case 2:
                    a.afisare_filme("devizionat.txt");
                    break;
                case 3:
                    c.dupa_titlu();
                    break;
                case 4:
                    c.dupa_id();
                    break;
                case 5:
                    c.dupa_cuvant_cheie();
                    break;
                case 6:
                    v.viz("vizionate.txt");
                    break;
                case 7:
                    v.viz("devizionat.txt");
                    break;
                case 8:
                    a.afisare_rezumat();
                    break;
                case 9:
                    loop=0;
                    break;
                default:
                    System.out.println("Nu exista aceasta comanda");
                    break;
            }
            System.out.println("\n");
        }
    }
}
