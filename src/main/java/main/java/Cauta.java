package main.java;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Aceasta clasa ajuta la cautarea filmelor dupa cele 3 criterii posibile: titlu, id sau cuvant cheie.
 * In principal abordeaza cerintele 3,4 si 5.
 * Si aceasta clasa se foloseste de clasa Tools.
 *
 * @author Mitu Alexandru
 */
public class Cauta {

    private Tools util=new main.java.Tools();

    /**
     * Aceasta functie ajuta la cautarea dupa titlu.
     * Este folosita la optiunea 3.
     * @throws IOException
     */
    public void dupa_titlu() throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String titlu;
        System.out.print("Introduceti titlu:");
        titlu=br.readLine(); //introducere titlu
        String jsonContent =util.cerere_api(2,titlu); //preluare continutul json cu ajutorul titlului
        util.parse_pt_cautare(jsonContent);
    }

    /**
     * Aceasta functie ajuta la cautarea dupa ID.
     * Este folosita la optiunea 4.
     * @throws IOException
     */
    public void dupa_id() throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String id;
        System.out.print("Introduceti Id IMDb:");
        id=br.readLine(); //introducere Id
        String jsonContent =util.cerere_api(1,id); //preluare continutul json cu ajutorul id-ului
        util.parse_pt_cautare(jsonContent);
    }

    /**
     * Aceasta functie ajuta la cautarea dupa cuvantul cheie.
     * Este folosita la optiunea 5.
     * @throws IOException
     */
    public void dupa_cuvant_cheie() throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String cuv;
        System.out.print("Introduceti cuvant: ");
        cuv=br.readLine(); //introducere cuvant cheie
        String jsonContent =util.cerere_api(3,cuv); //preluare continutul json cu ajutorul cuvantului cheie
        util.parse_cheie(jsonContent);
    }

}
