package main.java;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ToolsTest {

    @Test
    void cerere_api() {
        boolean test = true;
        Tools instance = new Tools();
        try {
            instance.cerere_api(1, "tt3501632"); //test pt id
        } catch (Exception e) {
            e.printStackTrace();
            test = false;
            assertTrue(test);
        }
    }
    @Test
    void parse_pt_title() {
            boolean test =true;
            Tools instance=new Tools();
            try{
                instance.parse_pt_titlu("{\"Title\":\"Friends\",\"Year\":\"1994–2004\",\"Rated\":\"TV-14\",\"Released\":\"22 Sep 1994\",\"Runtime\":\"22 min\",\"Genre\":\"Comedy, Romance\",\"Director\":\"N/A\",\"Writer\":\"David Crane, Marta Kauffman\",\"Actors\":\"Jennifer Aniston, Courteney Cox, Lisa Kudrow, Matt LeBlanc\",\"Plot\":\"Follows the personal and professional lives of six twenty to thirty-something-year-old friends living in Manhattan.\",\"Language\":\"English, Dutch, Italian, French\",\"Country\":\"USA\",\"Awards\":\"Won 1 Golden Globe. Another 71 wins & 211 nominations.\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BNDVkYjU0MzctMWRmZi00NTkxLTgwZWEtOWVhYjZlYjllYmU4XkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_SX300.jpg\",\"Ratings\":[{\"Source\":\"Internet Movie Database\",\"Value\":\"8.9/10\"}],\"Metascore\":\"N/A\",\"imdbRating\":\"8.9\",\"imdbVotes\":\"728,004\",\"imdbID\":\"tt0108778\",\"Type\":\"series\",\"totalSeasons\":\"10\",\"Response\":\"True\"}");
            } catch (Exception e)
            {
                e.printStackTrace();
                test=false;
                assertTrue(test);
            }
        }

    @Test
    void parse_pt_rezumat() {
        boolean test =true;
        Tools instance=new Tools();
        try{
            instance.parse_pt_rezumat("{\"Title\":\"Friends\",\"Year\":\"1994–2004\",\"Rated\":\"TV-14\",\"Released\":\"22 Sep 1994\",\"Runtime\":\"22 min\",\"Genre\":\"Comedy, Romance\",\"Director\":\"N/A\",\"Writer\":\"David Crane, Marta Kauffman\",\"Actors\":\"Jennifer Aniston, Courteney Cox, Lisa Kudrow, Matt LeBlanc\",\"Plot\":\"Follows the personal and professional lives of six twenty to thirty-something-year-old friends living in Manhattan.\",\"Language\":\"English, Dutch, Italian, French\",\"Country\":\"USA\",\"Awards\":\"Won 1 Golden Globe. Another 71 wins & 211 nominations.\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BNDVkYjU0MzctMWRmZi00NTkxLTgwZWEtOWVhYjZlYjllYmU4XkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_SX300.jpg\",\"Ratings\":[{\"Source\":\"Internet Movie Database\",\"Value\":\"8.9/10\"}],\"Metascore\":\"N/A\",\"imdbRating\":\"8.9\",\"imdbVotes\":\"728,004\",\"imdbID\":\"tt0108778\",\"Type\":\"series\",\"totalSeasons\":\"10\",\"Response\":\"True\"}");
        } catch (Exception e)
        {
            e.printStackTrace();
            test=false;
            assertTrue(test);
        }
    }

    @Test
    void parse_cheie() {
        boolean test =true;
        Tools instance=new Tools();
        try{
            instance.parse_cheie("{\"Search\":[{\"Title\":\"Thor\",\"Year\":\"2011\",\"imdbID\":\"tt0800369\",\"Type\":\"movie\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BOGE4NzU1YTAtNzA3Mi00ZTA2LTg2YmYtMDJmMThiMjlkYjg2XkEyXkFqcGdeQXVyNTgzMDMzMTg@._V1_SX300.jpg\"},{\"Title\":\"Thor: The Dark World\",\"Year\":\"2013\",\"imdbID\":\"tt1981115\",\"Type\":\"movie\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BMTQyNzAwOTUxOF5BMl5BanBnXkFtZTcwMTE0OTc5OQ@@._V1_SX300.jpg\"},{\"Title\":\"Thor: Ragnarok\",\"Year\":\"2017\",\"imdbID\":\"tt3501632\",\"Type\":\"movie\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BMjMyNDkzMzI1OF5BMl5BanBnXkFtZTgwODcxODg5MjI@._V1_SX300.jpg\"},{\"Title\":\"Team Thor\",\"Year\":\"2016\",\"imdbID\":\"tt6016776\",\"Type\":\"movie\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BMmY4NzA2OGUtYTc0MS00OTc0LThhYjAtZTJkMTI2MTgyNDY0XkEyXkFqcGdeQXVyNDQ5MDYzMTk@._V1_SX300.jpg\"},{\"Title\":\"Thor: Tales of Asgard\",\"Year\":\"2011\",\"imdbID\":\"tt1667903\",\"Type\":\"movie\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BMTcxOTU4NzIxMV5BMl5BanBnXkFtZTcwNzE3NjAxNQ@@._V1_SX300.jpg\"},{\"Title\":\"Team Thor: Part 2\",\"Year\":\"2017\",\"imdbID\":\"tt6599818\",\"Type\":\"movie\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BOTJkYmFkM2ItOGIwMS00YWRhLTg5ZGQtNDA0ZjcwNzA3ZjQxXkEyXkFqcGdeQXVyNDQ5MDYzMTk@._V1_SX300.jpg\"},{\"Title\":\"Almighty Thor\",\"Year\":\"2011\",\"imdbID\":\"tt1792794\",\"Type\":\"movie\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BMTcwNjI5MTAzNF5BMl5BanBnXkFtZTcwNTcyOTIwNQ@@._V1_SX300.jpg\"},{\"Title\":\"Thor: Hammer of the Gods\",\"Year\":\"2009\",\"imdbID\":\"tt1260572\",\"Type\":\"movie\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BNTFkMDA5ODEtMTQ1NC00NGRhLWE3ZGQtOGE5ZGJkMGI1ZDI5XkEyXkFqcGdeQXVyMjI1NjEzNjg@._V1_SX300.jpg\"},{\"Title\":\"Thor: Legend of the Magical Hammer\",\"Year\":\"2011\",\"imdbID\":\"tt1241721\",\"Type\":\"movie\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BMTYxOTI1OTI0NF5BMl5BanBnXkFtZTgwMDQzMjcwMzE@._V1_SX300.jpg\"},{\"Title\":\"Thor & Loki: Blood Brothers\",\"Year\":\"2011–\",\"imdbID\":\"tt1922373\",\"Type\":\"series\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BMTkxMTg1ODQ1MF5BMl5BanBnXkFtZTgwMDQ2ODcwMzE@._V1_SX300.jpg\"}],\"totalResults\":\"58\",\"Response\":\"True\"}");
        } catch (Exception e)
        {
            e.printStackTrace();
            test=false;
            assertTrue(test);
        }
    }
    @Test
    void parse_pt_cautare() {
        boolean test = true;
        Tools instance = new Tools();
        try {
            instance.parse_pt_cautare("{\"Title\":\"Friends\",\"Year\":\"1994–2004\",\"Rated\":\"TV-14\",\"Released\":\"22 Sep 1994\",\"Runtime\":\"22 min\",\"Genre\":\"Comedy, Romance\",\"Director\":\"N/A\",\"Writer\":\"David Crane, Marta Kauffman\",\"Actors\":\"Jennifer Aniston, Courteney Cox, Lisa Kudrow, Matt LeBlanc\",\"Plot\":\"Follows the personal and professional lives of six twenty to thirty-something-year-old friends living in Manhattan.\",\"Language\":\"English, Dutch, Italian, French\",\"Country\":\"USA\",\"Awards\":\"Won 1 Golden Globe. Another 71 wins & 211 nominations.\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BNDVkYjU0MzctMWRmZi00NTkxLTgwZWEtOWVhYjZlYjllYmU4XkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_SX300.jpg\",\"Ratings\":[{\"Source\":\"Internet Movie Database\",\"Value\":\"8.9/10\"}],\"Metascore\":\"N/A\",\"imdbRating\":\"8.9\",\"imdbVotes\":\"728,004\",\"imdbID\":\"tt0108778\",\"Type\":\"series\",\"totalSeasons\":\"10\",\"Response\":\"True\"}");
        } catch (Exception e) {
            e.printStackTrace();
            test = false;
            assertTrue(test);
        }
    }


}