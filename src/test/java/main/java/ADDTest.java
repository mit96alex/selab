package main.java;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ADDTest {

    @org.junit.jupiter.api.Test
    void viz() {
        boolean thrown = true;
        //functia de viz din ADD
            String id="tt3501632";
            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);
            try {
                FileWriter myWriter = new FileWriter("vizionate.txt",true); //deschiderea fisierului pentru a scrie id-ul filmului
                myWriter.write(id+"\n"); //scriere id in fisier
                myWriter.close();
                System.out.println("Done.");
            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
                thrown=false;
            }

        assertTrue(thrown);
    }
}